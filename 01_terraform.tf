
terraform {
  backend "s3" {
    bucket = "changeme"
    key    = "terransible"
    region = "eu-west-1"
  }
}

provider "aws" {
  region = "eu-west-1"
}