
resource "aws_instance" "db_server" {
  ami           = data.aws_ami.workshop_ubuntu_xenial.id
  instance_type = "t3.medium"
  tags = {
    Name = "terransible_db_server"
    Env = "production"
    Project = "terransible"
  }
  root_block_device {
    volume_size = "20"
  }
  key_name               = aws_key_pair.terransible_key.key_name
  subnet_id              = data.aws_subnet.workshop_subnet.id
  vpc_security_group_ids = [ data.aws_security_group.workshop_security_group.id ]
  connection {
    host        = coalesce(self.public_ip, self.private_ip)
    type        = "ssh"
    user        = "ubuntu"
    private_key = tls_private_key.terransible_key.private_key_pem
  }
}
