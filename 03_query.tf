
data "aws_ami" "workshop_ubuntu_xenial" {
  most_recent = true
  filter {
    name   = "name"
    values = ["devops-ubuntu-18-04-x64*"]
  }
  owners = ["self"]
}

data "aws_subnet" "workshop_subnet" {
  filter {
    name   = "tag:Name"
    values = [ "*-public-eu-west-1a" ]
  }
}

data "aws_security_group" "workshop_security_group" {
  name = "telia_security"
}

data "aws_availability_zones" "available" {
}
