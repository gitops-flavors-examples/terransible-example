
resource "tls_private_key" "terransible_key" {
  algorithm = "RSA"
}

resource "aws_key_pair" "terransible_key" {
  key_name   = "terransible_key"
  public_key = tls_private_key.terransible_key.public_key_openssh
}

resource "local_file" "server_pem" {
  content  = tls_private_key.terransible_key.private_key_pem
  filename = "outputs/server.pem"
  file_permission = "0400"
}
